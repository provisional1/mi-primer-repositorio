use bd_test;
create table bd_test.cliente_CG(
id_cliente int not null AUTO_INCREMENT,
nombre varchar(200) NOT NULL,
apellido varchar(64)NOT null,
cedula varchar(64) not null, 
sexo char(1)null,
fecha_nacimiento datetime not null,
 primary key(id_cliente),
 unique index idx_unique_cedula(cedula)
 )
 comment 'tabla de clientes de Christofer';
 
 create table bd_test.cliente_hobbie_CG(
 id_cliente int not null,
 id_hobbie int not null,
 primary key(id_cliente,id_hobbie)

 )
 
 comment 'Tabla de hobies de clientes de Christofer González';
 create table bd_test.hobbie_CG(
 id_hobbie int not null auto_increment,
 descripcion varchar(64) not null, 
 primary key(id_hobbie)
 )
 comment 'Tabla de categorias de hobbie de Christofer González';